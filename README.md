# Teste dev Ruby on Rails

Este teste consiste em consumir um webhook do Github e apresentar as informações conforme descrito abaixo.

1. Você deve ter uma conta no github e um repositório para poder configurar um webhook (https://developer.github.com/webhooks).
2. Você deve desenvolver um endpoint chamado /issues para receber os eventos através do webhook.
3. Você deve desenvolver um endpoint chamado /repositories/id/issues para listar as issues de um determinado repositório através do id do mesmo e exibir o resultado mostrado abaixo.
4. Você deve desenvolver um endpoint chamado /issues/id para exibir as informações de uma issue através do id da mesma e exibir o resultado mostrado abaixo.

###### /repositories/id/issues
```json
[
    {
        "id": 73464126,
        "action": "closed",
        "repository_id": 526,
        "repository_name": "hello-world",
        "owner_name": "github",
        "created_at": "2018-04-25T20:42:10Z",
        "updated_at": "2018-04-25T20:43:34Z"
    }
]
```

###### /issues/id
```json
{
    "id": 73464126,
    "action": "closed",
    "repository_id": 526,
    "repository_name": "hello-world",
    "owner_name": "github",
    "created_at": "2018-04-25T20:42:10Z",
    "updated_at": "2018-04-25T20:43:34Z"
}
```

### Observações

1. Você não pode usar gems sem ser as default do Ruby on Rails.
2. Você pode usar o ngrok para testar o webhook localmente.
3. Você deve escrever no arquivo readme.md as instruções para rodar o projeto e executar os testes.
4. Você deve versionar o projeto no bitbucket (privado) e adicionar como membro do projeto a lautaro.castillo@zaitt.com.br (a entrega será feita por lá).
5. Utilizar docker e docker-compose te dará uns pontos a mais.
6. Desenvolver uma interface para que o usuário consiga ver as issues de um repositório te dará uns pontos a mais.
